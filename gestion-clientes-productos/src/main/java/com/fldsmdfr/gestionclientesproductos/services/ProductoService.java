package com.fldsmdfr.gestionclientesproductos.services;

import com.fldsmdfr.gestionclientesproductos.entities.Producto;
import com.fldsmdfr.gestionclientesproductos.exceptions.ProductoNotFoundException;
import com.fldsmdfr.gestionclientesproductos.repositories.ProductoRepo;
import lombok.Data;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@Service
public class ProductoService {
    private final ProductoRepo productoRepo;
    /*
    CREATE
     */
    public Producto crearProducto(Producto producto){
        return productoRepo.save(producto);
    }

    /*
    READ
     */
    public Page<Producto> listarProducto(int size, int page, Boolean disponibilidad){
        Pageable pageable= PageRequest.of(size, page);
        return productoRepo.findByDisponibilidad(disponibilidad, pageable);
    }

    public List<Producto> obeterProductos(String nombre, Boolean disponibilidad){
        List<Producto> productos= productoRepo.findByNombre(nombre, disponibilidad);
        return productos;
    }
    
    public Producto obeterProducto(String nombre){
        Producto producto= productoRepo.findByNombre(nombre).orElseThrow(()->new RuntimeException("No escontrado"));
        return producto;
    }

    public Producto obtenerProductoById(int id){
        Optional<Producto> optionalProducto= productoRepo.findById(id);
        return optionalProducto.orElseThrow(() -> new ProductoNotFoundException("El producto "+id+ " no existe"));
    }

    /*
    UPDATE
     */
    public Producto actualizarProducto(int id, Producto productoRequest){
        Producto producto= productoRepo.findById(id).orElseThrow(() -> new ProductoNotFoundException("El producto no existe"));
        producto.setNombre(productoRequest.getNombre());
        producto.setPrecio(productoRequest.getPrecio());
        producto.setImagenUrl(producto.getImagenUrl());
        return productoRepo.save(producto);
    }

    public Producto cambiarDisponibilidad(Integer id, Boolean disponibilidad){
        Producto producto= productoRepo.findById(id).orElseThrow(() -> new ProductoNotFoundException("El producto no existe"));
        producto.setDisponibilidad(disponibilidad);
        return productoRepo.save(producto);
    }

    /*
    DELETE
     */
    public void eliminarProducto(int id){
        productoRepo.deleteById(id);
    }
}
