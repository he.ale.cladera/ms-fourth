package com.fldsmdfr.gestionclientesproductos.services;

import com.fldsmdfr.gestionclientesproductos.entities.Cliente;
import com.fldsmdfr.gestionclientesproductos.exceptions.ClienteNotFoundException;
import com.fldsmdfr.gestionclientesproductos.exceptions.ProductoNotFoundException;
import com.fldsmdfr.gestionclientesproductos.repositories.ClienteRepo;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@Service
public class ClienteService {
    private final ClienteRepo clienteRepo;

    /*
    CREATE
     */
    public Cliente crearCliente(Cliente cliente){
        return clienteRepo.save(cliente);
    }
    /*
    READ
     */
    public Page<Cliente> listarCliente(int page, int size, Boolean activo){
        Pageable pageable= PageRequest.of(page, size);
        return clienteRepo.findByActivo(activo, pageable);
    }

    public Cliente obtenerCliente(String email){
        Optional<Cliente> optionalCliente= clienteRepo.findClienteByEmail(email);
        return optionalCliente.orElseThrow(() -> new ClienteNotFoundException("email no encontrado"));
    }

    public Cliente obtenerClienteById(int id){
        Optional<Cliente> optionalCliente= clienteRepo.findById(id);
        return optionalCliente.orElseThrow(() ->  new ClienteNotFoundException("El cliente "+id+ " no existe"));
    }

    public List<Cliente> obtenerClienteByNit(String nit, Boolean activo){
        return clienteRepo.findClienteByNit(nit, activo);
    }

    public List<String> findNitClientes(String nit){
        return clienteRepo.findNitClientes(nit);
    }
    /*
    UPDATE
     */
    public Cliente actualizarCliente(int id, Cliente clienteRequest){
        Cliente cliente= clienteRepo.findById(id).orElseThrow(() -> new ClienteNotFoundException("cliente no encontrado"));
        cliente.setEmail(clienteRequest.getEmail());
        cliente.setNit(clienteRequest.getNit());
        cliente.setEmail(clienteRequest.getEmail());
        return clienteRepo.save(cliente);
    }

    public Cliente cambiarEstado(Integer id, Boolean estado){
        Cliente cliente= clienteRepo.findById(id).orElseThrow(() -> new ProductoNotFoundException("El cliente no existe"));
        cliente.setActivo(estado);
        return clienteRepo.save(cliente);
    }

    /*
    REMOVE
     */
    public void eliminarCliente(int id){
        clienteRepo.deleteById(id);
    }
}
