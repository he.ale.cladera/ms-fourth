package com.fldsmdfr.gestionclientesproductos.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@AllArgsConstructor
public class ExceptionResponse {
    private LocalDateTime fecha;
    private String mensaje;
    private String detalle;
}
