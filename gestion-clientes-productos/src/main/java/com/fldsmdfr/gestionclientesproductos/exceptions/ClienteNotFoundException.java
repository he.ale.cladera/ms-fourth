package com.fldsmdfr.gestionclientesproductos.exceptions;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
public class ClienteNotFoundException extends RuntimeException {
    public ClienteNotFoundException(String mensaje){
        super(mensaje);
    }
}
