package com.fldsmdfr.gestionclientesproductos.exceptions;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
public class ProductoNotFoundException extends RuntimeException {
    public ProductoNotFoundException(String mensaje){
        super(mensaje);
    }
}
