package com.fldsmdfr.gestionclientesproductos.repositories;

import com.fldsmdfr.gestionclientesproductos.entities.Producto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Repository
public interface ProductoRepo extends JpaRepository<Producto, Integer> {

    @Query("SELECT p FROM Producto p WHERE LOWER(p.nombre) LIKE LOWER(CONCAT('%', :nombre, '%')) AND p.disponibilidad= :disponibilidad")
    List<Producto> findByNombre(String nombre, Boolean disponibilidad);

    Page<Producto> findByDisponibilidad( Boolean disponibilidad, Pageable pageable);

    @Query("SELECT p FROM Producto p WHERE LOWER(p.nombre)= LOWER(:nombre) AND p.disponibilidad=true")
    Optional<Producto> findByNombre(String nombre);
}
