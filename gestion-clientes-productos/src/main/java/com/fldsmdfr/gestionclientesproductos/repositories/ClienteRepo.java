package com.fldsmdfr.gestionclientesproductos.repositories;

import com.fldsmdfr.gestionclientesproductos.entities.Cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Repository
public interface ClienteRepo extends JpaRepository<Cliente, Integer> {
    Optional<Cliente> findClienteByEmail(String email);

    @Query("SELECT cliente FROM Cliente cliente WHERE cliente.nit LIKE CONCAT(:nit, '%') AND cliente.activo= :activo")
    List<Cliente> findClienteByNit(String nit, boolean activo);

    @Query("SELECT c.nit FROM Cliente c WHERE c.nit LIKE CONCAT(:nit, '%') AND c.activo= true")
    List<String> findNitClientes(String nit);

    Page<Cliente> findByActivo(Boolean activo, Pageable pageable);
}
