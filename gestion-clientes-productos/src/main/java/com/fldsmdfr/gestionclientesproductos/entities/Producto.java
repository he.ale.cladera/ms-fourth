package com.fldsmdfr.gestionclientesproductos.entities;

import jakarta.persistence.*;
import lombok.Data;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@Entity
@Table(name = "producto")
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 100)
    private String nombre;
//    @Column(precision = 9, scale = 2)
    private Float precio;
    @Column(length = 255)
    private String imagenUrl;
    private Boolean disponibilidad;
}
