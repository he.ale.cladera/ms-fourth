package com.fldsmdfr.gestionclientesproductos.entities;

import jakarta.persistence.*;
import lombok.Data;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@Entity
@Table(name = "cliente")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 100)
    private String nombre;
    @Column(unique = true, length = 100)
    private String email;
    @Column(length = 20, unique = true)
    private String nit;
    private Boolean activo;
}
