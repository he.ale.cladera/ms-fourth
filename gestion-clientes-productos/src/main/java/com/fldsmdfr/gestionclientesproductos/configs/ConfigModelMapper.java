package com.fldsmdfr.gestionclientesproductos.configs;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Configuration
public class ConfigModelMapper {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
