package com.fldsmdfr.gestionclientesproductos.configs;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(
    info = @Info(
        title = "Gestion Clientes Productos Aplication",
        version = "1,0,0",
        description = "Esta aplicacion contiene opreciones de Clientes y Productos"
    )
)
public class OpenApiConfig {
    
}
