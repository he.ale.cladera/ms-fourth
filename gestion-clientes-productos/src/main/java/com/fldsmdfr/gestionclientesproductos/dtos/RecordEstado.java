package com.fldsmdfr.gestionclientesproductos.dtos;

public record RecordEstado(Boolean estado) {
}
