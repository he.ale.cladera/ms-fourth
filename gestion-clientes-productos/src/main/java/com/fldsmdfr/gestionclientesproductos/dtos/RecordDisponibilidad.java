package com.fldsmdfr.gestionclientesproductos.dtos;

public record RecordDisponibilidad(Boolean disponibilidad) {
}