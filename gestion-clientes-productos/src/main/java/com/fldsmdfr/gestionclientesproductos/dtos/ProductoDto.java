package com.fldsmdfr.gestionclientesproductos.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductoDto {
    private Integer id;
    @NotBlank(message = "El nombre del producto es obligatorio")
    @Size(max = 100, message = "El nombre del producto no puede tener más de 100 caracteres")
    private String nombre;
    @Positive(message = "El precio debe ser un valor positivo")
    private Double precio;
    private String url;
    private Boolean disponibilidad;
}
