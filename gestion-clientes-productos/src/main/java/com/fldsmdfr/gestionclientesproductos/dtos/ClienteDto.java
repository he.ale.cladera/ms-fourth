package com.fldsmdfr.gestionclientesproductos.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class ClienteDto {
    private Integer id;
    @NotBlank(message = "El nombre del cliente es obligatorio")
    @Size(max = 100, message = "El nombre del cliente no puede tener más de 100 caracteres")
    private String nombre;
    @NotBlank(message = "El email del cliente es obligatorio")
    @Size(max = 100, message = "El email del cliente no puede tener más de 100 caracteres")
    private String email;
    @NotBlank(message = "El nit del cliente es obligatorio")
    @Size(max = 20, message = "El nit del cliente no puede tener más de 20 caracteres")
    private String nit;
    private Boolean activo;
}
