package com.fldsmdfr.gestionclientesproductos.controllers;

import com.fldsmdfr.gestionclientesproductos.dtos.ClienteDto;
import com.fldsmdfr.gestionclientesproductos.dtos.RecordEstado;
import com.fldsmdfr.gestionclientesproductos.entities.Cliente;
import com.fldsmdfr.gestionclientesproductos.services.ClienteService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@RestController
@Data
@RequestMapping(path = "cliente")
@Tag(name = "Cliente")
public class ClienteController {
    private final ClienteService clienteService;
    private final ModelMapper mapper;

    /*
    GET
     */
    @Operation(description = "Lista a los clientes en un page")
    @GetMapping("/listar")
    public ResponseEntity<Page<ClienteDto>> listar(@RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "10") int size,
                                                   @RequestParam(defaultValue = "true")Boolean activo){
        Page<Cliente> clientePage= clienteService.listarCliente(page,size, activo);
        Page<ClienteDto> clienteDtoPage= clientePage.map(cliente -> mapper.map(cliente, ClienteDto.class));
        return new ResponseEntity<>(clienteDtoPage, HttpStatus.OK);
    }

    @Operation(description = "Lista a al cliente por email")
    @GetMapping("/buscar/{email}")
    public ResponseEntity<ClienteDto> buscarByEmail(@PathVariable String email){
        Cliente cliente= clienteService.obtenerCliente(email);
        ClienteDto response= mapper.map(cliente, ClienteDto.class);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Lista a al cliente por id")
    @GetMapping("/buscarId/{id}")
    public ResponseEntity<ClienteDto> buscarById(@PathVariable int id){
        Cliente cliente= clienteService.obtenerClienteById(id);
        ClienteDto response= mapper.map(cliente, ClienteDto.class);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Buscar a un cliente por nit")
    @GetMapping("/buscarNit")
    public ResponseEntity<List<ClienteDto>> buscarByNit(@RequestParam(defaultValue = "000000") String nit,
                                                        @RequestParam(defaultValue = "true") Boolean activo){
        List<Cliente> clientes= clienteService.obtenerClienteByNit(nit, activo);
        List<ClienteDto> response= clientes.stream().map(c-> mapper.map(c, ClienteDto.class)).toList();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(description = "Buscar los nit para la factura")
    @GetMapping("/buscarNitClientes")
    public ResponseEntity<List<String>> buscarByNit(@RequestParam(defaultValue = "000000") String nit){
        List<String> response= clienteService.findNitClientes(nit);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
    POST
     */
    @Operation(description = "Crea un nuevo cliente")
    @PostMapping("/crear")
    public ResponseEntity<Cliente> crearCliente(@RequestBody ClienteDto clienteDto){
        Cliente cliente= mapper.map(clienteDto, Cliente.class);
        Cliente response= clienteService.crearCliente(cliente);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    /*
    PUT
     */
    @Operation(description = "Actualiza el cliente")
    @PutMapping("/actualizar/{id}")
    public ResponseEntity<ClienteDto> actualizarCliente(@PathVariable int id, @RequestBody ClienteDto clienteDto){
        Cliente cliente= mapper.map(clienteDto, Cliente.class);
        Cliente clienteAcualizado= clienteService.actualizarCliente(id, cliente);
        ClienteDto response= mapper.map(clienteAcualizado, ClienteDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
     * PATCH
     */
    @Operation(description = "Desactiva a un cliente")
    @PatchMapping("/desactivar/{id}")
    public ResponseEntity<ClienteDto> desactivarCliente(@PathVariable int id, @RequestBody() RecordEstado recordEstado){
        Cliente cliente= clienteService.cambiarEstado(id, recordEstado.estado());
        ClienteDto response= mapper.map(cliente, ClienteDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
    DELETE
     */
    @Operation(description = "Elimina a un cliente")
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<Void> eliminarCliente(@PathVariable int id){
        clienteService.eliminarCliente(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
