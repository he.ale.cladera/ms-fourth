package com.fldsmdfr.gestionclientesproductos.controllers;

import com.fldsmdfr.gestionclientesproductos.dtos.ProductoDto;
import com.fldsmdfr.gestionclientesproductos.dtos.RecordDisponibilidad;
import com.fldsmdfr.gestionclientesproductos.entities.Producto;
import com.fldsmdfr.gestionclientesproductos.services.ProductoService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@RestController
@RequestMapping(path = "producto")
@Tag(name = "Producto")
public class ProductoController {
    public final ProductoService productoService;
    public final ModelMapper mapper;

    public ProductoController(ProductoService productoService, ModelMapper mapper){
        this.mapper = mapper;
        this.productoService = productoService;
    }
    /*
    GET
     */
    @Operation(description = "Lista a los productos en un page")
    @GetMapping(path = "/listar")
    public ResponseEntity<Page<ProductoDto>> listarProducto(@RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "10") int size,
                                                            @RequestParam(defaultValue = "true")Boolean disponibilidad){
        Page<Producto> productos= productoService.listarProducto(page, size,disponibilidad);
        Page<ProductoDto> response= productos.map(producto -> mapper.map(producto, ProductoDto.class));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(description = "Busca un producto por nombre")
    @GetMapping(path = "/buscar/{nombre}")
    public ResponseEntity<List<ProductoDto>> buscarPorductoByNombre(@PathVariable String nombre, 
                                                                    @RequestParam(defaultValue = "true")Boolean disponibilidad ){
        List<Producto> productos= productoService.obeterProductos(nombre, disponibilidad);
        List<ProductoDto> response= productos.stream().map(p->mapper.map(p, ProductoDto.class)).toList();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(description = "Busca un producto por nombre para registrar detalle")
    @GetMapping(path = "/buscarParaFactura/{nombre}")
    public ResponseEntity<ProductoDto> buscarPorductoByNombreParaDetalle(@PathVariable String nombre){
        Producto producto= productoService.obeterProducto(nombre);
        ProductoDto response= mapper.map(producto, ProductoDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(description = "Busca un producto por su Id")
    @GetMapping(path = "/buscarId/{id}")
    public ResponseEntity<ProductoDto> buscarPorductoByNombre(@PathVariable int id){
        Producto producto= productoService.obtenerProductoById(id);
        ProductoDto response= mapper.map(producto, ProductoDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
    POST
     */
    @Operation(description = "Crea un nuevo producto")
    @PostMapping(path =  "/crear")
    public ResponseEntity<ProductoDto> crearProducto(@RequestBody ProductoDto productoDto){
        Producto producto= mapper.map(productoDto, Producto.class);
        Producto nuevoProducto= productoService.crearProducto(producto);
        ProductoDto response= mapper.map(nuevoProducto, ProductoDto.class);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /*
    PUT
     */
    @Operation(description = "Actualiza un producto")
    @PutMapping(path = "/actualizar/{id}")
    public ResponseEntity<ProductoDto> actualizarProducto(@PathVariable int id, @RequestBody ProductoDto productoDto){
        Producto producto= mapper.map(productoDto, Producto.class);
        Producto productoActualizado= productoService.actualizarProducto(id, producto);
        ProductoDto response= mapper.map(productoActualizado, ProductoDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
     * PATCH
     */
    @Operation(description = "Da de baja a los productos por su id")
    @PatchMapping(path = "/disponibilidad/{id}")
    public ResponseEntity<Void> disponibilidadProducto(@PathVariable int id, @RequestBody RecordDisponibilidad disponibilidad){
        productoService.cambiarDisponibilidad(id, disponibilidad.disponibilidad());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
    DELETE
     */
    @Operation(description = "Elimina los productos por su id")
    @DeleteMapping(path = "/eliminar/{id}")
    public ResponseEntity<Void> eliminarProducto(@PathVariable int id){
        productoService.eliminarProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
