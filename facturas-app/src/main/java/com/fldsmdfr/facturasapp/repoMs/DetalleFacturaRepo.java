package com.fldsmdfr.facturasapp.repoMs;

import com.fldsmdfr.facturasapp.dtos.DetalleDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@FeignClient(name = "detalle-factura-app")
public interface DetalleFacturaRepo {
    @GetMapping(path = "/detalleFactura/listarDetalles/{idFactura}")
    List<DetalleDto> getDetalles(@PathVariable int idFactura);


    @PostMapping(path = "/detalleFactura/guardarDetalles")
    List<DetalleDto> guardarDetalles(@RequestBody List<DetalleDto> lista);
    
    @PostMapping(path = "/detalleFactura/guardar")
    DetalleDto guardarDetalle(@RequestBody DetalleDto detalle);

    @PutMapping(path = "/detalleFactura/editar/{id}")
    DetalleDto actualizar(@PathVariable int id, @RequestBody DetalleDto itemDto);

    @DeleteMapping(path = "/detalleFactura/eliminarPorFactura/{idFactura}")
    void eliminarDetalleFacturaByFactura(@PathVariable int idFactura);

    @DeleteMapping(path = "/detalleFactura/eliminar/{id}")
    void deleteById(@PathVariable int id);
}
