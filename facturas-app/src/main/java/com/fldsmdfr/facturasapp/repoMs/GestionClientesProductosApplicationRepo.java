package com.fldsmdfr.facturasapp.repoMs;

import com.fldsmdfr.facturasapp.dtos.ClienteDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@FeignClient(name = "gestion-clientes-productos")
public interface GestionClientesProductosApplicationRepo {

    @GetMapping(path = "/cliente/buscarNit")
    List<ClienteDto> buscarClienteByNit(@RequestParam() String nit);

    @GetMapping(path =  "/cliente/buscarId/{id}")
    ClienteDto buscarById(@PathVariable int id);

}
