package com.fldsmdfr.facturasapp.exceptions;

public class FacturaNotFoundException extends RuntimeException {
    public FacturaNotFoundException(String mensaje){
        super(mensaje);
    }
    
}
