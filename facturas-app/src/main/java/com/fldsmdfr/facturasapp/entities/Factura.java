package com.fldsmdfr.facturasapp.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@Data
@Entity
@Table(name = "factura")
public class Factura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nit_cliente")
    @JoinColumn(name = "nit_cliente", referencedColumnName = "id")
    private String nitCliente;
    private LocalDate fecha;
    private Double total;
}
