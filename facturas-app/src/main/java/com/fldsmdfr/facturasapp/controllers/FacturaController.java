package com.fldsmdfr.facturasapp.controllers;

import com.fldsmdfr.facturasapp.dtos.FacturaDetalladaDto;
import com.fldsmdfr.facturasapp.dtos.FacturaDto;
import com.fldsmdfr.facturasapp.dtos.FacturaReporteDto;
import com.fldsmdfr.facturasapp.entities.Factura;
import com.fldsmdfr.facturasapp.services.FacturaService;
import lombok.Data;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@Data
@RestController
@RequestMapping(path = "/factura")
public class FacturaController {
    private final FacturaService facturaService;
    private final ModelMapper mapper;

    @GetMapping(path = "/listar")
    public ResponseEntity<Page<FacturaDto>> listar(@RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "10") int size){
        Page<Factura> fPage= facturaService.listar(page, size);
        Page<FacturaDto> response= fPage.map(e-> mapper.map(e, FacturaDto.class));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(path = "/crearFactura")
    public ResponseEntity<FacturaDto> crearFactura(@RequestBody FacturaDetalladaDto facturaCrearDto){
        FacturaDto facturaDto= mapper.map(facturaCrearDto, FacturaDto.class);
        Factura request= mapper.map(facturaDto, Factura.class);
        Factura factura= facturaService.crearFactura(request, facturaCrearDto.getDetalles());
        FacturaDto response= mapper.map(factura, FacturaDto.class);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(path = "/obtener/{id}")
    public ResponseEntity<FacturaReporteDto> facturaDtoResponseEntity(@PathVariable int id){
        FacturaReporteDto facturaDto= facturaService.verFactura(id);
        return new ResponseEntity<>(facturaDto, HttpStatus.OK);
    }

    @PutMapping(path = "/actualizar/{id}")
    public ResponseEntity<FacturaDto> actualizarFacturaDtoResponseEntity(@PathVariable Integer id, @RequestBody FacturaDetalladaDto facturaActualizarDto) {
        Factura request = mapper.map(facturaActualizarDto, Factura.class);
        Factura factura = facturaService.actualizarFactura(id, request, facturaActualizarDto.getDetalles());
        FacturaDto response = mapper.map(factura, FacturaDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(path = "/eliminar/{id}")
    public ResponseEntity<Void> eliminarFactura(@RequestParam int id){
        facturaService.eliminarFactura(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
