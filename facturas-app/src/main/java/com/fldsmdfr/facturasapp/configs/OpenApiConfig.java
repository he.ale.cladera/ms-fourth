package com.fldsmdfr.facturasapp.configs;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "Facturas aplicacion",
                version = "1.0.0",
                description = "esta aplicacion se enacarga de gestionar las facturas"
        )
)
public class OpenApiConfig {
}
