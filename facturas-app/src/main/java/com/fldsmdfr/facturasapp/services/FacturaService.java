package com.fldsmdfr.facturasapp.services;

import com.fldsmdfr.facturasapp.dtos.ClienteDto;
import com.fldsmdfr.facturasapp.dtos.DetalleDto;
import com.fldsmdfr.facturasapp.dtos.FacturaReporteDto;
import com.fldsmdfr.facturasapp.entities.Factura;
import com.fldsmdfr.facturasapp.exceptions.FacturaNotFoundException;
import com.fldsmdfr.facturasapp.repoMs.DetalleFacturaRepo;
import com.fldsmdfr.facturasapp.repoMs.GestionClientesProductosApplicationRepo;
import com.fldsmdfr.facturasapp.repositories.FacturaRepo;
import lombok.Data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@Data
@Service
@Transactional
public class FacturaService {
    private final FacturaRepo facturaRepo;
    private final GestionClientesProductosApplicationRepo gestionClientesProductosApplicationRepo;
    private final DetalleFacturaRepo detalleFacturaRepo;
    /*
    CREATE
     */
    public Factura crearFactura(Factura factura, List<DetalleDto> detalles){
        List<ClienteDto> clienteDto= gestionClientesProductosApplicationRepo.buscarClienteByNit(factura.getNitCliente());
        ClienteDto cliente;
        if (clienteDto.isEmpty()){
            throw new RuntimeException("Usuario no encontrado");
        }else{
            cliente= clienteDto.get(0);
        }
        Factura f= new Factura();
        f.setFecha(factura.getFecha());
        f.setTotal(factura.getTotal());
        f.setNitCliente(cliente.getNit());
        facturaRepo.save(f);
        
        List<DetalleDto> listDetalles=detalles.stream().map(d->{
            DetalleDto detalleDto=new DetalleDto();
            detalleDto.setCantidad(d.getCantidad());
            detalleDto.setFacturaId(f.getId());
            detalleDto.setPrecioUnitario(d.getPrecioUnitario());
            detalleDto.setProductoNombre(d.getProductoNombre());
            detalleDto.setSubtotal(d.getSubtotal());
            return detalleDto;
        }).collect(Collectors.toList());
        try{
            detalleFacturaRepo.guardarDetalles(listDetalles);
        }catch(Exception e){
            facturaRepo.deleteById(f.getId());
            throw new RuntimeException("Ocurrio un error");
        }
        return facturaRepo.findById(f.getId()).orElseThrow();
    }

    /*
    READ
     */
    public FacturaReporteDto verFactura(int id){
        Factura factura= facturaRepo.findById(id).orElseThrow(()->new FacturaNotFoundException("La factura con id: " +id+ " no existe"));
        ClienteDto c= gestionClientesProductosApplicationRepo.buscarClienteByNit(factura.getNitCliente()).get(0);
        List<DetalleDto> detalles= detalleFacturaRepo.getDetalles(factura.getId());
        FacturaReporteDto facturaDto= FacturaReporteDto.builder()
                .cliente(c)
                .total(factura.getTotal())
                .fecha(factura.getFecha())
                .detalles(detalles)
                .id(factura.getId())
                .build();
        return facturaDto;
    }

    public Page<Factura> listar( int page, int size){
        Pageable pageable= PageRequest.of(page, size);
        return facturaRepo.findAll(pageable);
    }
    /*
    UPDATE
     */
    public Factura actualizarFactura(int id, Factura factura, List<DetalleDto> detalles){
        Factura facturaDb= facturaRepo.findById(id).orElseThrow(()->new FacturaNotFoundException("no encontrado"));
        facturaDb.setTotal(factura.getTotal());
        facturaDb.setFecha(facturaDb.getFecha());
        facturaDb.setNitCliente(factura.getNitCliente());
        HashSet<DetalleDto> otro= new HashSet<>();
        Factura update= facturaRepo.save(facturaDb);
        detalles.forEach(d->{
            DetalleDto aux;
            if (d.getId()>0)
                aux= detalleFacturaRepo.actualizar(d.getId(), d);
            else
                aux= detalleFacturaRepo.guardarDetalle(d);
            otro.add(aux);
        });
        HashSet<DetalleDto> detalleDB= new HashSet<>(detalleFacturaRepo.getDetalles(id));
        detalleDB.removeAll(otro);
        detalleDB.forEach(e-> detalleFacturaRepo.deleteById(e.getId()));
        return update;
    }

    /*
    DELETE
     */
    public void eliminarFactura(int id){
        detalleFacturaRepo.eliminarDetalleFacturaByFactura(id);
        facturaRepo.deleteById(id);
    }
}
