package com.fldsmdfr.facturasapp.dtos;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FacturaDetalladaDto {
    private Integer id;
    private LocalDate fecha;
    private Double total; 
    private String nitCliente;
    private List<DetalleDto> detalles;
}
