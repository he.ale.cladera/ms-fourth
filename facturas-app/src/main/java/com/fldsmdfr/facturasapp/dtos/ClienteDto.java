package com.fldsmdfr.facturasapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author M. Alejandro Cladera M.
 * @project gestion-clientes-productos
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ClienteDto {
    private Integer id;
    private String nombre;
    private String email;
    private String nit;
}
