package com.fldsmdfr.facturasapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DetalleDto {
    private Integer id;
    private Integer facturaId;
    private String productoNombre;
    private Integer cantidad;
    private Double precioUnitario;
    private Double subtotal;
} 
