package com.fldsmdfr.facturasapp.dtos;

import java.time.LocalDate;

import lombok.Data;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
public class FacturaDto {
    private Integer id;
    private LocalDate fecha;
    private Double total; 
    private String nitCliente;
}
