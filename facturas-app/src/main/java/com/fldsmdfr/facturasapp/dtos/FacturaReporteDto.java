package com.fldsmdfr.facturasapp.dtos;

import java.time.LocalDate;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FacturaReporteDto {
    private Integer id;
    private LocalDate fecha;
    private Double total; 
    private ClienteDto cliente;
    private List<DetalleDto> detalles;
}
