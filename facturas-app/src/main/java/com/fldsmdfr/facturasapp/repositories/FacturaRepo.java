package com.fldsmdfr.facturasapp.repositories;

import com.fldsmdfr.facturasapp.entities.Factura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@Repository
public interface FacturaRepo extends JpaRepository<Factura, Integer> {

}
