package com.fldsmdfr.gatewayapp.configurations;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author M. Alejandro Cladera M.
 * @project gateway-app
 */
@Configuration
public class Gateway {
    @Bean
    public RouteLocator routeLocatorEurekaOff(RouteLocatorBuilder builder){
        return builder
                .routes()
                .route(route->
                    route.path("/cliente/**","/producto/**")
                         .uri("lb://gestion-clientes-productos")
                )
                .route(route->route
                        .path("/factura/**")
                        .uri("lb://facturas-app")
                )
                .route(route->route
                        .path("/itemFactura/**")
                        .uri("lb://detalle-factura-app")
                )
                .build();
    }
}
