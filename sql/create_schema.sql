-- Tabla de Cliente
CREATE TABLE cliente (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(100),
    email VARCHAR(100) UNIQUE,
    nit VARCHAR(20) NOT NULL UNIQUE,
    activo BOOLEAN DEFAULT TRUE
);

-- Tabla de Producto
CREATE TABLE producto (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(100) UNIQUE,
    precio DECIMAL(10, 2),
    imagen_url TEXT,
    disponibilidad BOOLEAN DEFAULT TRUE
);

-- Tabla de Factura (Maestro)
CREATE TABLE factura (
    id SERIAL PRIMARY KEY,
    nit_cliente VARCHAR(20) DEFAULT 'C/F', -- Valor predeterminado 'C/F' (Cliente Ficticio)
    fecha DATE,
    total DECIMAL(10, 2)
);

-- Tabla de detalle de Factura (Detalle)
CREATE TABLE detalle_factura (
    id SERIAL PRIMARY KEY,
    factura_id INT,
    producto_nombre VARCHAR(100),
    cantidad INT,
    precio_unitario DECIMAL(10, 2),
    subtotal DECIMAL(10, 2)
); 
