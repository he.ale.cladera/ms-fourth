# Application

## Important notes about Data Base (db)

* About Data Base I created on Docker, I used Postgresql 16.1
* It's called "db-shop"
* Password, user, and db name is defined in enviroments
* DB port is "5432". However, you can change the port for example "5555:5432" and your computer will listen to the port "5555", but if you don't change the port your pc will listen to the port 5432. 

## Generar el jar
*enter the folder of each microservice and execute: mvn clean package -DskipTests
*if you can use docker execute (you can call whatever you want): 
* * docker build -t 413ale/server-api-facturacion:1.0.0 .
* * docker build -t 413ale/gestor-cliente-producto-api-facturacion:1.0.0 .