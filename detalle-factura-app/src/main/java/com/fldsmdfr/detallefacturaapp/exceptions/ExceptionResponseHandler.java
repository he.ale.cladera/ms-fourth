package com.fldsmdfr.detallefacturaapp.exceptions;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ExceptionResponseHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(DetalleFacturaNotFoundException.class)
    public ResponseEntity<ExceptionResponse> manejarClienteNotFoundException(DetalleFacturaNotFoundException ex,
                                                                            HttpHeaders headers,
                                                                            HttpStatusCode status,
                                                                            WebRequest request){
        ExceptionResponse er= new ExceptionResponse(LocalDateTime.now(),ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatusCode status,
                                                                  WebRequest request) {
        String mensaje= ex.getBindingResult().getAllErrors().stream().map(e->{
            return e.getDefaultMessage().concat(", ");
        }).collect(Collectors.joining());
        ExceptionResponse er= new ExceptionResponse(LocalDateTime.now(),mensaje, request.getDescription(false));
        return ResponseEntity.badRequest().body(er);
    }
}
