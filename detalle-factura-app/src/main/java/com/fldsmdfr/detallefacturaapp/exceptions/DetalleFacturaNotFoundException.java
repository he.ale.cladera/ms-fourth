package com.fldsmdfr.detallefacturaapp.exceptions;

public class DetalleFacturaNotFoundException extends RuntimeException {
    public DetalleFacturaNotFoundException(String mensaje){
        super(mensaje);
    }
    
}
