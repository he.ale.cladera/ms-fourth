package com.fldsmdfr.detallefacturaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class DetalleFacturaAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DetalleFacturaAppApplication.class, args);
    }

}
