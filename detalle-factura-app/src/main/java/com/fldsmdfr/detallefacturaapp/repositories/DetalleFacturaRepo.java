package com.fldsmdfr.detallefacturaapp.repositories;

import com.fldsmdfr.detallefacturaapp.entities.DetalleFactura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Repository
public interface DetalleFacturaRepo extends JpaRepository<DetalleFactura, Integer> {
    List<DetalleFactura> findItemFacturaByFacturaId(int facturaId);

    Integer deleteByFacturaId(Integer id);
}
