package com.fldsmdfr.detallefacturaapp.entities;

import jakarta.persistence.*;
import lombok.Data;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
@Entity
@Table(name = "detalle_factura")
public class DetalleFactura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "factura_id")
    @JoinColumn(name = "factura_id", referencedColumnName = "id")
    private Integer facturaId;
    @Column(name = "producto_nombre")
    @JoinColumn(name = "producto_nombre", referencedColumnName = "nombre")
    private String productoNombre;
    private Integer cantidad;
    @Column(name = "precio_unitario")
    private Double precioUnitario;
    private Double subtotal;
}
