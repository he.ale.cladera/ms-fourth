package com.fldsmdfr.detallefacturaapp.services;

import com.fldsmdfr.detallefacturaapp.dtos.ProductoDto;
import com.fldsmdfr.detallefacturaapp.entities.DetalleFactura;
import com.fldsmdfr.detallefacturaapp.repoMs.GestionClientesProductosApplicationRepo;
import com.fldsmdfr.detallefacturaapp.repositories.DetalleFacturaRepo;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
@Service
public class DetalleFacturaService {
    private final DetalleFacturaRepo detalleFacturaRepo;
    private final GestionClientesProductosApplicationRepo gestionClientesProductosApplicationRepo;
    /*
    CREATE
     */
    // crea en la base de datos los elementos de una lista
    public List<DetalleFactura> crearDetallesFactura(List<DetalleFactura> itemsFactura){
        List<DetalleFactura> detalleFacturas= detalleFacturaRepo.saveAll(itemsFactura);
        return detalleFacturas;
    }

    public DetalleFactura crearDetalleFactura(DetalleFactura detalleFactura){
        return detalleFacturaRepo.save(detalleFactura);
    }

    /*
    READ
     */
    // metodo para obtener los detalles de una factura
    public List<DetalleFactura> listarDetallesFactura(int idFactura){
        List<DetalleFactura> detalles=detalleFacturaRepo.findItemFacturaByFacturaId(idFactura);
        return detalles;
    }

    public DetalleFactura buscarDetalleFactura(int id){
        Optional<DetalleFactura> optionalDetalleFactura= detalleFacturaRepo.findById(id);
        DetalleFactura detalleFactura= optionalDetalleFactura.orElseThrow(() -> new RuntimeException("El detalle con el "+ id + " no existe"));
        
        return detalleFactura;
    }
    /*
    UPDATE
     */
    public DetalleFactura actualizarDetalleFactura(int id, DetalleFactura request){
        Optional<ProductoDto> productoOptional= Optional.of( gestionClientesProductosApplicationRepo.buscarProductoByName(request.getProductoNombre()));
        if(!productoOptional.isPresent())
            throw new RuntimeException("El producto no existe");        

        DetalleFactura detalleFactura= detalleFacturaRepo.findById(id).orElseThrow(() -> new RuntimeException("El detalle con el "+ id + " no existe"));
        detalleFactura.setCantidad(request.getCantidad());
        detalleFactura.setSubtotal(request.getSubtotal());
        detalleFactura.setPrecioUnitario(request.getPrecioUnitario());
        detalleFactura.setProductoNombre(request.getProductoNombre());

        return detalleFacturaRepo.save(detalleFactura);
    }
    /*
    DELETE
     */
    public void eliminarDetalleFactura(int id){
        detalleFacturaRepo.deleteById(id);
    }

    public void eliminarDetalleFacturaByFactura(int id){
        detalleFacturaRepo.deleteByFacturaId(id);
    }
}
