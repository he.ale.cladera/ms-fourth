package com.fldsmdfr.detallefacturaapp.controllers;

import com.fldsmdfr.detallefacturaapp.dtos.DetalleFacturaDto;
import com.fldsmdfr.detallefacturaapp.entities.DetalleFactura;
import com.fldsmdfr.detallefacturaapp.services.DetalleFacturaService;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
@RestController
@RequestMapping(path = "/detalleFactura")
public class detalleFacturaController {
    private final DetalleFacturaService detalleFacturaService;
    private final ModelMapper mapper;

    /*
    GET
     */
    @GetMapping(path = "/listarDetalles/{idFactura}")
    public ResponseEntity<List<DetalleFacturaDto>> listarItems(@PathVariable int idFactura){
        List<DetalleFacturaDto> detalleFacturaList= detalleFacturaService.listarDetallesFactura(idFactura).stream().map(e->mapper.map(e, DetalleFacturaDto.class)).toList();
        return new ResponseEntity<>(detalleFacturaList, HttpStatus.OK);
    }

    @GetMapping(path = "/detalle/{id}")
    public ResponseEntity<DetalleFacturaDto> findItem(@PathVariable int id){
        DetalleFactura detalle = detalleFacturaService.buscarDetalleFactura(id);
        DetalleFacturaDto response= mapper.map(detalle, DetalleFacturaDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
    POST
     */
    @PostMapping(path = "/guardarDetalles")
    public ResponseEntity<List<DetalleFacturaDto>> guardarDetalles(@RequestBody List<DetalleFacturaDto> itemFacturaDtoList){
        List<DetalleFactura> detalleFacturaList= itemFacturaDtoList.stream().map(e->mapper.map(e, DetalleFactura.class)).toList();
        List<DetalleFactura> detalles=detalleFacturaService.crearDetallesFactura(detalleFacturaList);
        List<DetalleFacturaDto> response= detalles.stream().map(e->mapper.map(e, DetalleFacturaDto.class)).toList();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping(path = "/guardar")
    public ResponseEntity<DetalleFacturaDto> guardatDetalle(@RequestBody DetalleFacturaDto detalleDto){
        DetalleFactura request= mapper.map(detalleDto, DetalleFactura.class);
        DetalleFactura detalle=detalleFacturaService.crearDetalleFactura(request);
        DetalleFacturaDto response= mapper.map(detalle, DetalleFacturaDto.class);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /*
    PUT
     */
    @PutMapping(path = "/editar/{id}")
    public ResponseEntity<DetalleFacturaDto> editarItem(@PathVariable int id, @RequestBody DetalleFacturaDto detalleFacturaDto){
        DetalleFactura detalleFactura= mapper.map(detalleFacturaDto, DetalleFactura.class);
        DetalleFactura detalleActualizado= detalleFacturaService.actualizarDetalleFactura(id, detalleFactura);
        DetalleFacturaDto response= mapper.map(detalleActualizado, DetalleFacturaDto.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
    DELETE
     */
    @DeleteMapping(path = "/eliminar/{id}")
    public ResponseEntity<Void> eliminarDetalle(@PathVariable int id){
        detalleFacturaService.eliminarDetalleFactura(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(path = "/eliminarPorFactura/{idFactura}")
    public ResponseEntity<Void> eliminarDetalleFacturaByFactura(@PathVariable int idFactura){
        detalleFacturaService.eliminarDetalleFacturaByFactura(idFactura);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}