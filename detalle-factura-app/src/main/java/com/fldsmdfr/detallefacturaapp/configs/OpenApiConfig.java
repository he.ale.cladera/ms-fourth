package com.fldsmdfr.detallefacturaapp.configs;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "Detalle Factura Aplication",
                version = "1,0,0",
                description = "Esta aplicacion contiene opreciones de Detalles Factura"
        )
)
public class OpenApiConfig {
}
