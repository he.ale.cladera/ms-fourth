package com.fldsmdfr.detallefacturaapp.dtos;

import lombok.Data;

/**
 * @author M. Alejandro Cladera M.
 * @project detalle-factura-app
 */
@Data
public class DetalleFacturaDto {
    private Integer id;
    private Integer facturaId;
    private String productoNombre;
    private Integer cantidad;
    private Double precioUnitario;
    private Double subtotal;
}