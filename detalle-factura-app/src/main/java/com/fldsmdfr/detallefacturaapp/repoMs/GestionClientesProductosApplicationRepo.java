package com.fldsmdfr.detallefacturaapp.repoMs;

import com.fldsmdfr.detallefacturaapp.dtos.ProductoDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author M. Alejandro Cladera M.
 * @project facturas-app
 */
@FeignClient(name = "gestion-clientes-productos")
public interface GestionClientesProductosApplicationRepo {

    @GetMapping(path = "/producto/buscarParaFactura/{nombre}")
    ProductoDto buscarProductoByName(@PathVariable String nombre);

}
